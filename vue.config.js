module.exports = {
  publicPath: './',
  outputDir: 'dist',
  assetsDir: 'assets',
  devServer: {
    overlay: false,
    port: 8080,// 端口
    // proxy : 'http://localhost:5556'
    proxy: {
      '/api': {
        target: 'http://localhost:5556', // 代理接口地址
        secure: false,  // 如果是https接口，需要配置这个参数
        changeOrigin: true, // 是否跨域
        pathRewrite: {
          '^/api': ''   //需要rewrite的, 这里理解成以'/api'开头的接口地址，把/api代替target中的地址
        }
      }
    }
  },
  productionSourceMap: !process.env.NODE_ENV == "production",

  css: {
    sourceMap: !process.env.NODE_ENV == "production",
  },
  configureWebpack: {

  },
  chainWebpack: config => {
    /* 配置scss全局mixin */
    const scss = config.module.rule('scss').oneOfs.store
    scss.forEach(item => {
      item
        .use('sass-resources-loader')
        .loader('sass-resources-loader')
        .options({
          resources: 'src/assets/style/mixin/mixin.scss'
        })
        .end()
    })
  }
}