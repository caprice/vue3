/*****
// 路由最多两层

// name 名称
// router 路由地址
// icon 图标名称
// key 路由key 不可相同
// color 图标颜色
// children 子menu 
            如果children是对象 则进行分组 对象的key就为 分组名 值为数组 是子menu
            如果children是数字 则为子menu
*****/
const list: menuItem[] = [
  {
    name: "首页",//现实的名字
    icon: "Expand",//menu显示的图标
    key: "/home",//跳转到哪 对应的route的路由
    translate: {
      en: 'HOME',
    }
  },
  {
    name: "权限管理",
    icon: "Expand",
    key: "/jurisdiction",
    translate: {
      en: 'Authority management'
    },
    children: {
      '一组': [{//此处无法翻译 
        name: "用户管理",
        icon: "Expand",
        key: "/user",
        translate: {
          en: 'User management'
        },
      },
      {
        name: "角色管理",
        icon: "Expand",
        key: "/role",
        translate: {
          en: 'Role management'
        },
      },
      ]
    }
  },
]
export interface menuItem {
  name: string,
  router?: string,
  icon: string,
  key: string,
  color?: string,
  translate?: {
    [key: string]: string
  },
  children?: menuItem[] | { [key: string]: menuItem[] }
}

export function translateFun(language: string = 'en') {
  // 扁平化
  const flatFun = (data: menuItem[]): any => {
    let list: {
      [key: string]: string
    } = {}
    if (Object.keys(data).length) {
      if (Array.isArray(data)) {
        for (let i of data) {
          if (i.children) {
            console.log(i.key);
            Object.assign(list, {
              ...flatFun(i.children as menuItem[])
            })
            list[i.name] = i.translate ? i.translate[language] ? i.translate[language] : i.name : i.name
          } else {
            list[i.name] = i.translate ? i.translate[language] ? i.translate[language] : i.name : i.name
          }
        }
      } else {
        for (let i of Object.keys(data)) {
          if (data[i]) {
            Object.assign(list, {
              ...flatFun(data[i] as menuItem[])
            })
          }
        }
      }
    }
    return list
  }
  return flatFun(list)
}


export default list
