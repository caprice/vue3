import { createI18n } from "vue-i18n";
import en from "./en";
import zhCn from "./zhCn";

export const i18n = createI18n({
  legacy: false,
  locale: JSON.parse(localStorage.getItem("cache") || "").locale ?? "zhCn", //默认语言
  messages: {
    en,
    zhCn,
  },
});
export const setLocale = (value: any) => {
  i18n.global.locale.value = value;
};
export default i18n;
