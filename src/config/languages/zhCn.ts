import config from '../projectOptions'
import { translateFun } from '@/config/menuConfig'
export default {
  login: {
    record: '记住密码',
    signIn: '登录',
    name: config.name,
    account: '账号',
    accountTips: '请输入账号',
    password: '密码',
    passwordTips: '请输入密码',
    passwordLengthTips: '密码长度6-11位',
    verificationErrorTips: '请确认验证码是否有效',
    verificationEmptyTips: '请填写验证码',
  },
  menu: {
    home: '主页',
    ...translateFun('zhCn')

  }
}