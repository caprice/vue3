import config from '../projectOptions'
import { translateFun } from '@/config/menuConfig'

export default {
  login: {
    record: 'Remember the password',
    signIn: 'Sign in',
    name: config.englishName,
    account: 'account',
    accountTips: 'Please enter the account number',
    password: 'password',
    passwordTips: 'Please input a password',
    passwordLengthTips: 'Password length 6-11 bits',
    verificationErrorTips: 'Please fill in the verification code',
    verificationEmptyTips: 'Please verify',
  },
  menu: {
    home: 'home',
    ...translateFun('en')
  }
}