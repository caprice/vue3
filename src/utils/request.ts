import axios from 'axios'
const request = axios.create({
  baseURL: '/mock',
  timeout: 5000,
  headers: {}
});

request.interceptors.response.use(function (response) {
  if (response.status == 200) {
    return response.data.data
  }
  // 对响应数据做点什么
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error);
});

export default request
