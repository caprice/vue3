/* eslint-disable */
declare module '@'
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  import { createI18n } from "vue-i18n"
  export default component
}
