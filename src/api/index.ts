import request from "@/utils/request";
const login = (data: any = {}) => {
  return request.get("login", {
    data
  })
}
export default {
  login
}